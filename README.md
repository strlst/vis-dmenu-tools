# vis dmenu-tools

This repository contains a lua plugin meant to be used for [vis](https://github.com/martanne/vis), for further information please refer to the [wiki](https://github.com/martanne/vis/wiki/Plugins).
To use, simply require the plugin:

```lua
require('plugins/dmenu-tools')
```

By default, adds two NORMAL mode shortcuts as motions for some of the added commands:

|shortcut|command|
|---|---|
|`<C-o>`|`dmenu-pick-current`|
|`<C-p>`|`dmenu-browse-current`|
||`dmenu-pick`|
||`dmenu-browse`|

A quick glance at the strings contained by `commands` in `dmenu-tools.lua` reveals the functionality and also serves as a central data structure for customization. In essence, `dmenu-tools` aims to explore how `vis` can be improved using readily available POSIX utilities in addition to `dmenu`.

NOTE: quite obviously requires `dmenu` and `vis`
